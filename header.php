<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Archivo+Black&family=Montserrat:wght@500;800&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="./public/style/index.css">
    <link rel="stylesheet" href="./public/style/realisation.css">
    <link rel="stylesheet" href="./public/style/services.css">
    <link rel="stylesheet" href="./public/style/histoire.css">
    <link rel="stylesheet" href="./public/style/contact.css">
    <title>Document</title>
</head>

<body>
    <header>
        <div class="logo">
            <img src="./public/images/logoHeader.gif" alt="votre logo">
        </div>
        <nav>
            <ul id="ulDesk">
                <li><a href="./index.php">Accueil</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Réalisation</a></li>
                <li><a href="#">Histoire</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </nav>
        <div class="social-icons">
            <a href="#"><i class="fab fa-facebook">
                    <img src="./public/images/facebookHeader.png" alt="" srcset="">
                </i></a>
            <a href="#"><i class="fab fa-twitter">
                    <img src="./public/images/instagramHeader.png" alt="" srcset="">
                </i></a>
            <a href="#"><i class="fab fa-instagram">
                    <img src="./public/images/whatsappHeader.png" alt="" srcset="">
                </i></a>
        </div>
        <div id="contH">
            <div class="l l1"></div>
            <div class="l l2"></div>
            <div class="l l3"></div>
        </div>
        <nav id="navMob">
            <ul id="ulMob">
                <li id="liMob1"><a href="./index.php">Accueil</a></li>
                <li id="ulMob2"><a href="#">Services <span id="plusHeader" class="colorBr">+</span><span id="moinHeader" class="colorBr">-</span></a></li>
                <li id="liMob2" class="posGrilleMob1 liMob"><a href="./service1.php">Finition et <span class="colorBr">décoration</span></a></li>
                <li id="liMob3" class="posGrilleMob2 liMob"><a href="#">Chalet et traitement <span class="colorBr">bois</span></a></li>
                <li id="liMob4" class="posGrilleMob3 liMob"><a href="#">Rénovation</a></li>
                <li id="liMob5"><a href="./histoire.php">Histoire</a></li>
                <li id="liMob6"><a href="./realisation.php">Réalisation</a></li>
                <li id="liMob7"><a href="./contact.php"> Contact</a></li>
            </ul>
        </nav>
    </header>