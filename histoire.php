<?php
include "./header.php";
?>
<section id="sct1Histoire">
    <img alt="" id="fond1HistoireSct1">
    <h1>Notre <span class="colorBr">histoire</span></h1>
    <p>La couleur des murs reflète l'âme d'un bâtiment.</p>
    <p>Nous mettons notre expertise en peinture, revêtements, rénovation et décoration à votre disposition pour tous vos projets, qu'ils soient intérieurs ou extérieurs</p>
</section>
<section id="sct2Histoire">
    <div id="ctnGen1HistoireSct2">
        <div id="ctn1LeftHistoireSct2">
            <h2>Chez nous la peinture, une véritable passion.</h2>
            <p>Nous mettons notre expertise en peinture, revêtements, rénovation et décoration à votre disposition pour tous vos projets, qu'ils soient intérieurs ou extérieurs</p>
        </div>
        <div id="ctn1RightHistoireSct2">
            <img src="./public/images/img1HistoireSct2.png" alt="" srcset="">
        </div>
    </div>
    <div id="ctnGen2HistoireSct2">
        <div id="ctn2LeftHistoireSct2">
            <img src="./public/images/img2HistoireSct2.png" alt="" srcset="">
        </div>
        <div id="ctn3RightHistoireSct2">
            <h2>Chez nous la peinture, une véritable passion.</h2>
            <p>Nous mettons notre expertise en peinture, revêtements, rénovation et décoration à votre disposition pour tous vos projets, qu'ils soient intérieurs ou extérieurs</p>
        </div>
    </div>
</section>
<?php
include "./footer.php";
?>
<script src="./public/script/histoire.js"></script>


