<footer>
    <img id="fondFooter" alt="" srcset="">
    <div id="divGen1Footer">
        <div id="ssCtnindex1">
            <div id="ssSsCtn1Footer">
                <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                <a href="">Contact</a>
            </div>
            <div id="ssSsCtn1Footer">
                <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                <a href="">Politique de confidentialité et des données personnelles</a>
            </div>
            <div id="ssSsCtn1Footer">
                <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                <a href="">Mentions légales</a>
            </div>
            <div id="ssSsCtn1Footer">
                <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                <a href=''>Plan du site</a>
            </div>
        </div>
        <div id="ssCtnindex2">
            <div id="ctnSocialFooter">
                <a href="">
                    <img src="./public/images/facebookHeader.png" alt="" srcset="">
                </a>
                <a href="#">
                    <img src="./public/images/instagramHeader.png" alt="" srcset="">
                </a>
                <a href="#">
                    <img src="./public/images/whatsappHeader.png" alt="" srcset="">
                </a>
            </div>
            <a href="">
                <p>Dylanestienne@gmail.com</p>
            </a>
            <p>05000</p>
        </div>
    </div>
    <div id="divGen2Footer">
        <p>Killian Vincent Développeur copyright@ 2023 tout droits réservés.</p>
    </div>
</footer>
<script src="./public/script/headerFooter.js"></script>