</section>
    <?php
    include "./header.php";
    ?>
    <section id="sct1Rea">
        <img id="fondRea1sct1" alt="" srcset="">
        <div id="ctnGen1ReaSct1">
            <h1><span class="borderB1">Découvrer nos </span><span class="borderB2">réalisations</span></h1>
            <p>En tant que peintre en bâtiment, j'ai réalisé avec succès de nombreux projets de rénovation, en apportant
                ma créativité et mon expertise technique pour créer des espaces uniques et satisfaire les besoins de mes
                clients.</p>
        </div>
        <div id="ctn1ReaSct1">
            <img alt="" id="fondRea2Sct1">
        </div>
    </section>
    <section id="sct2Rea">
        <p>Nos <span class="colorBr">réalisations</span></p>
        <div id="ctnGen1ReaSct2">
            <div id="ssCtn1GenReaSct2">
                <img src="./public/images/img3index8.png" alt="" srcset="">
                <h1 class="close">Design</h1>
                <p class="close">Protection,rénovation, peinture, design.</p>
            </div>
            <div id="ssCtn1GenReaSct2">
                <img src="./public/images/img3index8.png" alt="" srcset="">
                <h1 class="close">Design</h1>
                <p class="close">Protection,rénovation, peinture, design.</p>
            </div>
            <div id="ssCtn1GenReaSct2">
                <img src="./public/images/img3index8.png" alt="" srcset="">
                <h1 class="close">Design</h1>
                <p class="close">Protection,rénovation, peinture, design.</p>
            </div>
            <div id="ssCtn1GenReaSct2">
                <img src="./public/images/img3index8.png" alt="" srcset="">
                <h1 class="close">Design</h1>
                <p class="close">Protection,rénovation, peinture, design.</p>
            </div>
        </div>
        <div id="ctnGen2ReaSct2">
            <p>Découvrire <span class="colorBr">plus</span> </p>
            <svg viewBox="0 0 24 24">
                <path fill="#B48F6D" d="M12 20l-8-8h16z" />
            </svg>
        </div>
    </section>
    <?php
    include "./footer.php";
    ?>
    <script src="./public/script/realisation.js"></script>
    
    </body>
    </html>