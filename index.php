<?php
include "./header.php";
?>

<section id="sctIndex1" class="sectionColor">
    <img id="fond1Index1" alt="">
    <div id="ctnGblobIndex1">
        <div id="ctnIndex1_1">
            <div id="itemLeftIndex1">

            </div>
        </div>
        <div id="ctnIndex1_2">
            <div id="ctnCenterIndex1Deskp">
                <h1><span class="colorBr">Votre</span> peintre d’intérieur</h1>
                <p class="p1Index1">Des couleurs éclatantes et un travail de qualité supérieure pour votre bâtiment.
                    Contactez-nous pour une transformation rapide et professionnelle.</p>
                <button>Contactez</button>
            </div>
            <div id="ctnCenterIndex1Mob">
                <div id="ssCtn1Index1Mob">
                    <h2><span class="colorBr">Votre</span> peintre d’intérieur</h2>
                    <svg width="285" height="25" viewBox="0 0 285 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M284.5 23H224.5L221.5 19L219.5 21L217.5 16.5L216 17.5L214.5 14.5H211.5L206.5 7L205 9.5L200 3L194 12L193 9L187.5 16.5V14.5L181.5 21V17.5L174.5 23H0.5" stroke="#B48F6D" stroke-width="3" />
                    </svg>
                </div>
                <div id="ssCtn2Index1Mob">
                    <div class="itemSsCtn2IndexMob">
                        <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                        <p>Peinture</p>
                    </div>
                    <div class="itemSsCtn2IndexMob">
                        <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                        <p>Rénovation</p>
                    </div>
                    <div class="itemSsCtn2IndexMob">
                        <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                        <p>Décoration</p>
                    </div>

                </div>
                <button>En savoir plus +</button>
            </div>
        </div>
        <div id="ctnIndex1_3">
            <div id="itemRightIndex1">

            </div>
        </div>
    </div>
</section>
<section id="sctIndex2" class="sectionColor">
    <div id="ctnGblobIndex2">
        <div id="ctnIndex2_1">
            <img id="shadowAnim" src="./public/images/indexPage2.jpg" alt="" srcset="">
        </div>
        <div id="ctnIndex2_2">
            <div id="itemIndex2_1">
                <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                <p>Votre peintre d’exception</p>
            </div>
            <p id="itemIndex2_2">
                La couleur des murs reflète l'âme d'un bâtiment.
            </p>
            <p id="itemIndex2_3">Nous mettons notre expertise en peinture, revêtements, rénovation et décoration à
                votre disposition pour tous vos projets, qu'ils soient intérieurs ou extérieurs</p>
            <div id="itemIndex2_4">
                <img src="./public/images/btnPeinture.png" alt="" srcset="">
            </div>
        </div>
    </div>
</section>
<section id="sctIndex3" class="sectionColor">
    <img id="fondPage3" alt="" srcset="">
    <div class="ctnTitreIndex">
        <div id="ssIndex3_1">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
        </div>
        <p>Profitez de nos services</p>
    </div>
    <div id="ctnIndex3_2">
        <div id="ssCtnIndex3_1">
            <div class="ssIndex3_1">
                <img src="./public/images/item1Index3.gif" alt="" srcset="">
            </div>
            <p class="p2Index3">Satisfaction clients</p>
            <p class="p3Index3">Nous sommes dévoués à la satisfaction de nos clients en offrant un service
                personnalisé et en
                répondant à leurs besoins spécifiques.</p>
        </div>
        <div id="ssCtnIndex3_2">
            <div class="ssIndex3_1">
                <img src="./public/images/item2Index3.gif" alt="" srcset="">
            </div>
            <p class="p4Index3">Profitez de nos services</p>
            <p class="p5Index3">Nous sommes constamment à la recherche de nouvelles idées et de solutions innovantes
                pour offrir des projets uniques et originaux.</p>
        </div>
        <div id="ssCtnIndex3_3">
            <div class="ssIndex3_1">
                <img src="./public/images/item3Index3.gif" alt="" srcset="">
            </div>
            <p class="p6Index3">Respect des délais</p>
            <p class="p7Index3">Nous nous engageons à respecter les délais de réalisation de chaque projet, tout en
                assurant une qualité optimale de travail.</p>
        </div>
    </div>
    <div id="ctnIndex3_3">
        <p class="p8Index3">Nous sommes à vos côtés pour réaliser tous vos projets de rénovation, décoration et
            aménagement, en veillant à la qualité de nos produits et au respect de l'environnement.</p>
        <div class="ctnBtnPeint">
            <img src="./public/images/btnPeinture.png" alt="" srcset="">
        </div>
    </div>
</section>
<section id="sctIndex4" class="sectionColor">
    <div id="ctnGenIndexQct4">
        <div class="ssCtnIndex4">
            <p><span class="colorBr classAnimIndexPlus">+</span>3</p>
            <P>Années d'expérience</P>
        </div>
        <div class="ssCtnIndex4">
            <p><span class="colorBr classAnimIndexPlus">+</span>15</p>
            <P>Clients satisfait</P>
        </div>
        <div class="ssCtnIndex4">
            <p><span class="colorBr classAnimIndexPlus">+</span>780</p>
            <P>Heures de travail</P>
        </div>
    </div>
</section>
<section id="sctIndex5" class="sectionColor">
    <img id="fondIndex5" alt="" srcset="">
    <div class="ctnTitreIndex">
        <div id="ssIndex3_1">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
        </div>
        <p>Notre équipes</p>
    </div>
    <div id="ctnGenIndex5">
        <div id="ssCtnLeftIndex5">
            <img src="./public/images/imgIndex5.jpg" alt="" srcset="">
        </div>
        <div id="ssCtnRightIndex5">
            <p>Découvrer notre équipes de peintre</p>
            <p>Chez nous, chaque membre de l'équipe est impliqué dans la vie de l'entreprise pour vous offrir un
                service personnalisé et de qualité. En puisant dans notre expérience et notre savoir-faire, nous
                créons ensemble des projets uniques qui vous ressemblent.</p>
            <div class="ctnBtnPeint">
                <img src="./public/images/btnPeinture.png" alt="" srcset="">
            </div>
        </div>
    </div>
</section>
<section id="sctIndex6" class="sectionColor">
    <div class="ctnTitreIndex">
        <div id="ssIndex6_1">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
        </div>
        <p>Notre équipement</p>
    </div>
    <img id="fondIndex6_1" alt="" srcset="">
    <div id="ctnGenIndex6">
        <div id="ssCtn1Index6">
            <div id="ssSSctn1Index6">
                <p class="colorBr">Notre équipement</p>
                <p>Profitez des dernières technologies</p>
                <div id="ctnSpanIndex6">
                    <img src="./public/images/index6_2.png" alt="" srcset="">
                </div>
                <p>Précision, rapidité, performante et polyvalente.</p>
                <p>Cette machine à projection de peinture offre une utilisation précise et une protection de qualité
                    pour les travaux de peinture professionnels.</p>
            </div>
        </div>
        <div id="ssCtn2Index6">
            <img id="fondIndex6_2" alt="" srcset="">
        </div>
    </div>
</section>
<section id="sctIndex7" class="sectionColor">
    <div class="ctnGen1Index7">
        <img id="animIcone" src="./public/images/iconeIndexMob.png" alt="" srcset="">
        <p>Aptitude technique</p>
    </div>
    <div class="ctn2Index7 ctn3Index7">
        <div class="ctnGen1Index7Mob">
            <p>Finition et décoration. </p>
            <img src="./public/images/img2Index8Mob.png" alt="" srcset="">
        </div>
        <div class="ssCtnIndex7">
            <div class="ssSsCtnIndex71_1">
                <p>Finition et décoration.</p>
            </div>
        </div>
        <div id="rtest">
            <button>Voir plus <span class="colorBr">+</span></button>
        </div>
        <img class="imgIndex7 imgIndex7Ctn1_1" id="imgIndex7Ctn1_1" alt="" srcset="">
        <img class="imgIndex7 imgIndex7Ctn1_2" id="imgIndex7Ctn1_2" alt="" srcset="">
        <button>Voir <span class="colorBr">+</span></button>
    </div>
    <div class="ctn2Index7 ctn3Index7">
        <div class="ctnGen1Index7Mob">
            <p>Chalet et bois.</p>
            <img src="./public/images/img2Index8Mob.png" alt="" srcset="">
        </div>
        <div class="ssCtnIndex7">
            <div class="ssSsCtnIndex71_1">
                <p>Chalet et bois.</p>
            </div>
        </div>
        <div id="rtest">
            <button>Voir plus <span class="colorBr">+</span></button>
        </div>
        <img class="imgIndex7 imgIndex7Ctn1_1" id="imgIndex7Ctn1_3" alt="" srcset="">
        <img class="imgIndex7 imgIndex7Ctn1_2" id="imgIndex7Ctn1_4" alt="" srcset="">
        <button>Voir <span class="colorBr">+</span></button>
    </div>
    <div class="ctn2Index7 ctn3Index7">
        <div class="ctnGen1Index7Mob">
            <p>Rénovation.</p>
            <img src="./public/images/img2Index8Mob.png" alt="" srcset="">
        </div>
        <div class="ssCtnIndex7">
            <div class="ssSsCtnIndex71_1">
                <p>Rénovation.</p>
            </div>
        </div>
        <div id="rtest">
            <button>Voir plus <span class="colorBr">+</span></button>
        </div>
        <img class="imgIndex7 imgIndex7Ctn1_1" id="imgIndex7Ctn1_5" alt="" srcset="">
        <img class="imgIndex7 imgIndex7Ctn1_2" id="imgIndex7Ctn1_6" alt="" srcset="">
        <button>Voir <span class="colorBr">+</span></button>
    </div>

    <div id="ctn2Index7_3"></div>
</section>
<section id="sctIndex8" class="sectionColor">
    <div id="ctnGen1Index8">
        <img id="fond1Page3" alt="" srcset="">
    </div>
    <div id="ctnGen2Index8">
        <div id="ssCtnGenIndex8_1">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
        </div>
        <div id="ssCtnGenIndex8_2">
            <p>Mes réalisations.</p>
        </div>
        <div id="ssCtnGenIndex8_3">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
        </div>
    </div>
    <div id="ctnGen3Index8">
        <div id="item1Ctn3Index8">
            <img id="rea1Img1Index8" alt="" srcset="">
            <div id="ctnItem1Ctn3Index8">
                <p>Voir toutes nos réalisations</p>
                <img id="iconeIndex8" src="./public/images/iconeIndex8.png" alt="" srcset="">
            </div>
        </div>
        <div id="item2Ctn3Index8">
            <img id="rea2Img1Index8" alt="" srcset="">
            <div id="ctnItem1Ctn3Index8">
                <p>Voir toutes nos réalisations</p>
                <img id="iconeIndex8" src="./public/images/iconeIndex8.png" alt="" srcset="">
            </div>
        </div>
        <div id="item3Ctn3Index8">
            <img id="rea3Img1Index8" alt="" srcset="">
            <div id="ctnItem1Ctn3Index8">
                <p>Voir toutes nos réalisations</p>
                <img id="iconeIndex8" src="./public/images/iconeIndex8.png" alt="" srcset="">
            </div>
            <div id="item4Ctn3Index8"></div>
        </div>
    </div>
    <div id="ctnGen4Index8">
        <div id="ctnGen5Index8">
            <svg>
                <rect x="0" y="0" width="100%" height="5" fill="black" />
            </svg>
        </div>
        <div id="ctnGen6Index8">
            <a href="">Voir toutes nos réalisations <span class="colorBr">+</span></a>
        </div>
</section>
<section id="sctIndex9" class="sectionColor">
    <div id="ctnGenIndex9">
        <div id="ctnLeftIndex9">
            <button class="btnAvis" id="btnAvisLeft">&#8249;</button>
        </div>
        <div id="ctnCenterIndex9">
            <img id="imgAvisIndex" alt="" srcset="">
            <h2>Nos avis</h2>
            <p id="paraJsAvis"></p>
            <p id="nomJsAvis"></p>
        </div>
        <div id="ctnRightIndex9">
            <button class="btnAvis" id="btnAvisRight">&#8250;</button>
        </div>
    </div>
</section>
<?php
include "./footer.php";
?>
<script src="./public/script/script.js"></script>
<script src="./public/script/scroll.js"></script>

</body>

</html>