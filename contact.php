</section>
    <?php
    include "./header.php";
    ?>
    <section id="sct1Contact">
        <img src="./public/images/fond1Contact1.png" alt="" srcset="">
        <h2>Notre équipe se chargera au mieux de vous répondre </h2>
        <img src="./public/images/iconePosContact.png" alt="" srcset="">
        <a href="#">11 rue du marbre Gap (05000)</a>
        <img src="./public/images/iconePhoneContact.png" alt="" srcset="">
        <a href="">+33 6 76 96 98 97</a>

        <img id="img5Contact1" alt="">
    </section>
    <section id="sct2Contact">
        <div id="ctn1Sct2Contact">
            <div id="ssctn1Sct2Contact">
                <h2>Nous <span class="colorBr">contactez</span></h2>
                <p>Il est important pour notre équipe de connaitre vos besoins visuels ainsi que technique afin de vous
                    proposer la solution la plus adapter à votre situation et à vos envies personnelles.</p>
            </div>
            <div id="ssctn2Sct2Contact">
                <form action="">
                    <input placeholder="Nom" type="text" name="" id="">
                    <input placeholder="Prénom" type="text" name="" id="">
                    <input placeholder="Numéro" type="text" name="" id="">
                    <select aria-placeholder="Types de travaux" name="Type de traveaux" id="cars" form="carform">
                        <option value="volvo">Types de travaux</option>
                        <option value="saab">Saab</option>
                        <option value="opel">Opel</option>
                        <option value="audi">Audi</option>
                    </select>
                    <input placeholder="Message" type="text" name="" id="">
                    <button>Envoyé</button>
                </form>
            </div>
        </div>
        <div id="ctn2Sct2Contact">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15352.00961018873!2d6.073837283673064!3d44.56556818658788!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sfr!4v1683885028766!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

        </div>
    </section>
    <?php
    include "./footer.php";
    ?>
    <script src="./public/script/contact.js"></script>
    
    </body>
    </html>