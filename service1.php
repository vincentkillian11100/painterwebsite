</section>
    <?php
    include "./header.php";
    ?>

    <section id="sct1Service1">
        <div class="ctnGen1Service1Sct1">
            <h1>Protection, <span class="colorBr">finition</span> et <span class="colorBr">décoration</span></h1>
            <img id="img1Service1Sct1" alt="" srcset="">
        </div>
        <div class="ctnGen2Service1Sct1">
            <img id="fond1Service1Sct1" alt="" srcset="">
        </div>
    </section>
    <section class="sct2Service">
        <img id="fond1Service2" alt="" srcset="">
        <div class="ctnGen1Service">
            <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
            <h2>Nos atout</h2>
        </div>
        <div class="sctnGen2Service">
            <div class="ssCtn2Service2">
                <img src="./public/images/img1gen2sct2Service.png" alt="">
                <h3>Des finitions impeccables </h3>
                <p>Les finitions et décorations de peinture d'un peintre en bâtiment professionnel peuvent être
                    réalisées avec précision, permettant une finition uniforme et professionnelle.</p>
            </div>
            <div class="ssCtn2Service2">
                <img src="./public/images/img2gen2sct2Service.png" alt="" srcset="">
                <h3>Des décorations murales audacieuse</h3>
                <p>Les décorations murales complexes peuvent être créées avec une grande flexibilité en utilisant des
                    techniques de peinture modernes.</p>
            </div>
            <div class="ssCtn2Service2">
                <img src="./public/images/img3gen2sct2Service.png" alt="" srcset="">
                <h3>Faire des murs une œuvre d'art</h3>
                <p>La texture, la brillance et les motifs peuvent être ajoutés à une surface peinte pour créer des
                    finitions de peinture uniques et intéressantes.</p>
            </div>
        </div>
        <div class="sctnGen3Service">
            <img id="imgCtn3ServiceSct2" alt="" srcset="">
        </div>
    </section>
    <section class="sct3Service">
        <div class="ctnGen1ServiceSct3">
            <div class="ssctnGen1ServiceSct3">
                <img src="./public/images/img1Sct3Service.jpg" alt="" srcset="">
            </div>
            <div class="ssctnGen2ServiceSct3">
                <div class="ssSs1ctnGen2ServiceSct3">
                    <p>Nos types de solutions</p>
                    <img src="./public/images/img2Index8Mob.png" alt="" srcset="">
                </div>
                <div class="ssSs2ctnGen2ServiceSct3">
                    <div class="itemCtnGen2ServiceSct3">
                        <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium doloribus aspernatur ratione aliquam doloremque delectus inventore incidunt, eum voluptatem quod, tempore optio. Sunt co</p>
                    </div>
                    <div class="itemCtnGen2ServiceSct3">
                        <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium doloribus aspernatur ratione aliquam doloremque delectus inventore incidunt, eum voluptatem quod, tempore optio. Sunt co</p>
                    </div>
                    <div class="itemCtnGen2ServiceSct3">
                        <img src="./public/images/iconeIndexMob.png" alt="" srcset="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium doloribus aspernatur ratione aliquam doloremque delectus inventore incidunt, eum voluptatem quod, tempore optio. Sunt co</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    include "./footer.php";
    ?>
    <script src="./public/script/services.js"></script>
    
    </body>
    </html>